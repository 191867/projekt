package com.endpoint.sensor;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import com.service.sensor.SensorService;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Lukasz on 2017-03-19.
 */
@Component
@Path("/sensor")
public class SensorEndpoint {

    final static Logger logger = Logger.getLogger(SensorEndpoint.class);


    private SensorService sensorServiceImpl;



    @Autowired
    public void setSensorServiceImpl(SensorService sensorService){
        this.sensorServiceImpl = sensorService;
    }

    @GET
    @Produces(MediaType.APPLICATION_XML)
    @Path("/setOn/{workerId}/{roomId}")
    public Response sensorSetOnForWorkerByRoom(@PathParam("workerId") Long workerId, @PathParam("roomId") Long roomId){
        logger.info("worker " + workerId + " enter room " + roomId);
        try {
            sensorServiceImpl.sensorSetOnForWorkerByRoom(workerId, roomId);
        }catch(Exception e){
            e.printStackTrace();
            return Response.serverError().build();
        }
        return Response.ok().build();
    }

    @GET
    @Produces(MediaType.APPLICATION_XML)
    @Path("/setOff/{workerId}/{roomId}")
    public Response sensorSetOffForWorkerByRoom(@PathParam("workerId") Long workerId, @PathParam("roomId") Long roomId){
        logger.info("worker " + workerId + " exit room " + roomId);
        sensorServiceImpl.sensorSetOffForWorkerByRoom(workerId, roomId);
        return Response.ok().build();
    }

}
