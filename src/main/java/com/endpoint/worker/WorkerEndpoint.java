package com.endpoint.worker;

import org.apache.log4j.Logger;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by Lukasz on 2017-03-11.
 */
@Path("/worker")
public class WorkerEndpoint {

    final static Logger logger = Logger.getLogger(WorkerEndpoint.class);

    @GET
    @Produces(MediaType.APPLICATION_XML)
    @Path("/{workerId}")
    public Response getWorker(@PathParam("workerId") Long workerId){

        logger.info("odpytanie o workera");

        return Response.ok().build();
    }
}
