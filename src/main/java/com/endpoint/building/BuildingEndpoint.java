package com.endpoint.building;

import org.apache.log4j.Logger;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by Lukasz on 2017-03-11.
 */
@Path("building")
public class BuildingEndpoint {

    final static Logger logger = Logger.getLogger(BuildingEndpoint.class);
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/msg")
    public String worker(){
        return this.getClass().getSimpleName().toString();
    }
}
