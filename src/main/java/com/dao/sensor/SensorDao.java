package com.dao.sensor;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;


/**
 * Created by Lukasz on 2017-03-19.
 */
@Component
public class SensorDao {

    @Autowired
    private SessionFactory sessionFactory;

    public void sensorSetOnForWorkerByRoom(Long workerId, Long roomId) {
        Query qry = sessionFactory.getCurrentSession().createSQLQuery("insert into TabRegisterEntity(ID_PERSON, ID_SENSOR, DIRECTION) values(:workerId, :roomId, :typId)");
        qry.setParameter("workerId", workerId);
        qry.setParameter("roomId", roomId);
        qry.setParameter("typId", 1);
        qry.executeUpdate();
    }

    public void sensorSetOffForWorkerByRoom(Long workerId, Long roomId) {

    }

    public void setSessionFactory(SessionFactory sessionFactory) {
    }
}