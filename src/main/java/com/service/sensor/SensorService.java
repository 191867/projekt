package com.service.sensor;

/**
 * Created by Lukasz on 2017-03-19.
 */
public interface SensorService {
    public void sensorSetOnForWorkerByRoom(Long workerId, Long roomId);
    public void sensorSetOffForWorkerByRoom(Long workerId, Long roomId);
}
