package com.service.sensor;

import com.dao.sensor.SensorDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
/**
 * Created by Lukasz on 2017-03-19.
 */
@Component
public class SensorServiceImpl implements SensorService {

    @Autowired
    private SensorDao sensorDao;


    public void sensorSetOnForWorkerByRoom(Long workerId, Long roomId){
        sensorDao.sensorSetOnForWorkerByRoom(workerId, roomId);
    }

    public void sensorSetOffForWorkerByRoom(Long workerId, Long roomId){
        sensorDao.sensorSetOffForWorkerByRoom(workerId, roomId);
    }

    public void setSensorDao(SensorDao sensorDao) {
    }
}
