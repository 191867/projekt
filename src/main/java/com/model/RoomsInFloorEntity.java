package com.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Lukasz on 2017-03-16.
 */
@Entity
@Table(name = "rooms_in_floor", schema = "projekt", catalog = "")
public class RoomsInFloorEntity {
    private int idFloor;
    private int idRoom;

    @Basic
    @Column(name = "ID_FLOOR")
    public int getIdFloor() {
        return idFloor;
    }

    public void setIdFloor(int idFloor) {
        this.idFloor = idFloor;
    }

    @Basic
    @Column(name = "ID_ROOM")
    public int getIdRoom() {
        return idRoom;
    }

    public void setIdRoom(int idRoom) {
        this.idRoom = idRoom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoomsInFloorEntity that = (RoomsInFloorEntity) o;

        if (idFloor != that.idFloor) return false;
        if (idRoom != that.idRoom) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idFloor;
        result = 31 * result + idRoom;
        return result;
    }
}
