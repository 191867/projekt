package com.model;

import javax.persistence.*;

/**
 * Created by Lukasz on 2017-03-16.
 */
@Entity
@Table(name = "tab_building", schema = "projekt", catalog = "")
public class TabBuildingEntity {
    private int id;
    private String name;
    private int numberOfFloors;

    @Id
    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "NUMBER_OF_FLOORS")
    public int getNumberOfFloors() {
        return numberOfFloors;
    }

    public void setNumberOfFloors(int numberOfFloors) {
        this.numberOfFloors = numberOfFloors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TabBuildingEntity that = (TabBuildingEntity) o;

        if (id != that.id) return false;
        if (numberOfFloors != that.numberOfFloors) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + numberOfFloors;
        return result;
    }
}
