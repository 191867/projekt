package com.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Lukasz on 2017-03-16.
 */
@Entity
@Table(name = "sensors_in_room", schema = "projekt", catalog = "")
public class SensorsInRoomEntity {
    private int idRoom;
    private int idSensor;

    @Basic
    @Column(name = "ID_ROOM")
    public int getIdRoom() {
        return idRoom;
    }

    public void setIdRoom(int idRoom) {
        this.idRoom = idRoom;
    }

    @Basic
    @Column(name = "ID_SENSOR")
    public int getIdSensor() {
        return idSensor;
    }

    public void setIdSensor(int idSensor) {
        this.idSensor = idSensor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SensorsInRoomEntity that = (SensorsInRoomEntity) o;

        if (idRoom != that.idRoom) return false;
        if (idSensor != that.idSensor) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idRoom;
        result = 31 * result + idSensor;
        return result;
    }
}
