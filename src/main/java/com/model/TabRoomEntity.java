package com.model;

import javax.persistence.*;

/**
 * Created by Lukasz on 2017-03-16.
 */
@Entity
@Table(name = "tab_room", schema = "projekt", catalog = "")
public class TabRoomEntity {
    private int id;
    private String name;
    private int maxNumberOfPeople;
    private int numberOfExits;

    @Id
    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "MAX_NUMBER_OF_PEOPLE")
    public int getMaxNumberOfPeople() {
        return maxNumberOfPeople;
    }

    public void setMaxNumberOfPeople(int maxNumberOfPeople) {
        this.maxNumberOfPeople = maxNumberOfPeople;
    }

    @Basic
    @Column(name = "NUMBER_OF_EXITS")
    public int getNumberOfExits() {
        return numberOfExits;
    }

    public void setNumberOfExits(int numberOfExits) {
        this.numberOfExits = numberOfExits;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TabRoomEntity that = (TabRoomEntity) o;

        if (id != that.id) return false;
        if (maxNumberOfPeople != that.maxNumberOfPeople) return false;
        if (numberOfExits != that.numberOfExits) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + maxNumberOfPeople;
        result = 31 * result + numberOfExits;
        return result;
    }
}
