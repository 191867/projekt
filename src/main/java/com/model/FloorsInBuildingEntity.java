package com.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Lukasz on 2017-03-16.
 */
@Entity
@Table(name = "floors_in_building", schema = "projekt", catalog = "")
public class FloorsInBuildingEntity {
    private int idBuilding;
    private int idFloor;

    @Basic
    @Column(name = "ID_BUILDING")
    public int getIdBuilding() {
        return idBuilding;
    }

    public void setIdBuilding(int idBuilding) {
        this.idBuilding = idBuilding;
    }

    @Basic
    @Column(name = "ID_FLOOR")
    public int getIdFloor() {
        return idFloor;
    }

    public void setIdFloor(int idFloor) {
        this.idFloor = idFloor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FloorsInBuildingEntity that = (FloorsInBuildingEntity) o;

        if (idBuilding != that.idBuilding) return false;
        if (idFloor != that.idFloor) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idBuilding;
        result = 31 * result + idFloor;
        return result;
    }
}
