package com.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;

/**
 * Created by Lukasz on 2017-03-16.
 */
@Entity
@Table(name = "tab_register", schema = "projekt", catalog = "")
public class TabRegisterEntity {
    private int idPerson;
    private int idSensor;
    private int direction;
    private Timestamp time;

    @Basic
    @Column(name = "ID_PERSON")
    public int getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(int idPerson) {
        this.idPerson = idPerson;
    }

    @Basic
    @Column(name = "ID_SENSOR")
    public int getIdSensor() {
        return idSensor;
    }

    public void setIdSensor(int idSensor) {
        this.idSensor = idSensor;
    }

    @Basic
    @Column(name = "DIRECTION")
    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    @Basic
    @Column(name = "TIME")
    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TabRegisterEntity that = (TabRegisterEntity) o;

        if (idPerson != that.idPerson) return false;
        if (idSensor != that.idSensor) return false;
        if (direction != that.direction) return false;
        if (time != null ? !time.equals(that.time) : that.time != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idPerson;
        result = 31 * result + idSensor;
        result = 31 * result + direction;
        result = 31 * result + (time != null ? time.hashCode() : 0);
        return result;
    }
}
